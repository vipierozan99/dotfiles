# Install Dunst

Install Dunst
`sudo apt get install dunst`

Disable gnome service

```bash
sudo mv /usr/share/dbus-1/services/org.gnome.Shell.Notifications.service /usr/share/dbus-1/services/org.gnome.Shell.Notifications.service.disabled
```

Try to run dunst, it will show you the pid of gnome default notification daemon, then kill it
`kill <PID>`

Enable the service

`systemctl enable --user dunst.service`

Start it

`systemctl start --user dunst.service`

Restart to apply config changes

`systemctl restart --user dunst.service`
